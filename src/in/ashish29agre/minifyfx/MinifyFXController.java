/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.minifyfx;

import in.ashish29agre.minifyfx.util.JarFilesFilter;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;

/**
 *
 * @author AshishRAgre
 */
public class MinifyFXController implements Initializable {

    private static Logger log = Logger.getLogger(MinifyFXController.class.getName());

    @FXML
    private BorderPane borderPane;
    @FXML
    private Button loadJarBtn;
    @FXML
    private Button srcBtn;
    @FXML
    private Button destBtn;
    @FXML
    private TextField jarFilePathTextField;
    @FXML
    private TextField srcJs;
    @FXML
    private TextField destJs;
    @FXML
    private ListView<File> srcListView;
    @FXML
    private Button minifyBtn;

    private ObservableList<File> srcListItems;
    private ObservableList<File> destListItems;
    private DirectoryChooser jarDirChooser;
    private DirectoryChooser srcDirChooser;
    private DirectoryChooser destDirChooser;
    private File srcFile;
    private File destFile;
    private JsCompressor jsCompressor;

    @FXML
    private void handleButtonAction(ActionEvent event) {
        String btnText = "";
        if (event.getSource() instanceof Button) {
            Button clickedBtn = (Button) event.getSource();
            btnText = clickedBtn.getText();
        } else if (event.getSource() instanceof MenuItem) {
            MenuItem clickedBtn = (MenuItem) event.getSource();
            btnText = clickedBtn.getText();
        }
        switch (btnText) {
            case Constants.LOAD_JAR:
            case Constants.OPEN_JAR:
                loadJar();
                break;
            case Constants.SRC:
                loadSrc();
                break;
            case Constants.DEST:
                loadDest();
                break;
            case Constants.MINIFY:
                log.info("Something else" + srcListItems.size());
                if (jsCompressor != null) {
                    final ProgressDialog dialog = new ProgressDialog(true, Constants.TITLE_PROGRESS);
                    dialog.show();
                    jsCompressor.setProgressIndicator(dialog.getProgressIndicator());
                    jsCompressor.setStatus(dialog.getStatus());
                    jsCompressor.setJarPath(jarFilePathTextField.getText());
                    jsCompressor.bindProgress();
                    jsCompressor.bindStatus();
                    jsCompressor.start();

                }
                break;
            case Constants.ABOUT:
                DialogFX dialog = new DialogFX(DialogFX.Type.INFO);
                dialog.setTitle(Constants.ABOUT);
                dialog.setMessage(Constants.YUI_COMPRESSOR);
                dialog.showDialog();
                break;

        }
    }

    private void loadJar() {
        final File jarDir = jarDirChooser.showDialog(borderPane.getScene().getWindow());
        if (jarDir != null) {
            File[] returnedFiles = getFiles(jarDir.getAbsolutePath(), Constants.DOT_JAR, true);
            File fLatestJar = getLatestJar(returnedFiles);
            if (fLatestJar != null) {
                log.info("Latest jar is " + fLatestJar);
                jarFilePathTextField.setText(fLatestJar.getAbsolutePath());
                srcBtn.setDisable(false);
                destBtn.setDisable(true);
            } else {
                DialogFX dialog = new DialogFX(DialogFX.Type.ERROR);
                dialog.setTitle(Constants.TITLE_ERROR);
                dialog.setMessage("Could not find yui comnpressor jar in current " + jarDir.getName() + " folder");
                dialog.showDialog();
                srcBtn.setDisable(true);
                destBtn.setDisable(true);
            }
        } else {
            log.info("Directory chooser cancelled");
            DialogFX dialog = new DialogFX(DialogFX.Type.ERROR);
            dialog.setTitle(Constants.TITLE_ERROR);
            dialog.setMessage("YUI Compressor jar path not specified");
            dialog.showDialog();
            srcBtn.setDisable(true);
            destBtn.setDisable(true);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        log.setLevel(Level.OFF);
        srcJs.setEditable(false);
        destJs.setEditable(false);
        jarFilePathTextField.setEditable(false);
        srcListItems = FXCollections.observableArrayList();
        destListItems = FXCollections.observableArrayList();
        jarDirChooser = new DirectoryChooser();
        srcDirChooser = new DirectoryChooser();
        destDirChooser = new DirectoryChooser();
        jarDirChooser.setTitle(Constants.PATH_TO_YUI_COMPRESSOR);
        srcDirChooser.setTitle(Constants.JS_SRC_FOLDER);
        destDirChooser.setTitle(Constants.JS_DEST_FOLDER);
        jarDirChooser.setInitialDirectory(new File(System.getProperty(Constants.HOME_DIR)));
        srcDirChooser.setInitialDirectory(new File(System.getProperty(Constants.HOME_DIR)));
        destDirChooser.setInitialDirectory(new File(System.getProperty(Constants.HOME_DIR)));
        jsCompressor = new JsCompressor(srcListItems);
        jsCompressor.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

            @Override
            public void handle(WorkerStateEvent t) {
                Integer failed = (Integer) t.getSource().getValue();
                DialogFX dialog = new DialogFX(DialogFX.Type.INFO);
                dialog.setTitle(Constants.TITLE_INFORMATION);
                String msg = "Successfully minified " + (srcListItems.size() - failed) + " file(s)";

                if (failed > 0) {
                    msg = msg + "\nFailed to minify" + failed + " file(s)";
                }
                dialog.setMessage(msg);
                dialog.showDialog();
                srcListItems.clear();
                minifyBtn.setDisable(true);
                destBtn.setDisable(true);
                jsCompressor.reset();
            }
        });
        srcBtn.setDisable(true);
        destBtn.setDisable(true);
        minifyBtn.setDisable(true);
    }

    private File[] getFiles(String folderPath, String extension, boolean isJar) {
        JarFilesFilter jarFilter = new JarFilesFilter(extension, isJar);
        File path = new File(folderPath);
        return path.listFiles(jarFilter);
    }

    private File getLatestJar(File[] files) {
        int i = 0;
        int j = 0;
        while (i < files.length) {
            j = 0;
            while (j < files.length) {
                int x = files[i].getName().compareTo(files[j].getName());
                if (x < 0) {
                    File temp = files[i];
                    files[i] = files[j];
                    files[j] = temp;
                }
                j++;
            }
            i++;
        }
        if (files.length > 0) {
            return files[files.length - 1];
        } else {
            return null;
        }
    }

    private void loadSrc() {
        srcListItems.clear();
        final File srcDir = srcDirChooser.showDialog(borderPane.getScene().getWindow());
        if (srcDir != null) {
            srcFile = srcDir;
            srcJs.setText(srcDir.getAbsolutePath());
            File[] returnedFiles = getFiles(srcDir.getAbsolutePath(), Constants.DOT_JS, false);
            for (File f : returnedFiles) {
                log.info("File " + f.getName());
                srcListItems.add(f);
            }
            if (srcListItems.size() == 0) {
                DialogFX dialog = new DialogFX(DialogFX.Type.ERROR);
                dialog.setTitle(Constants.TITLE_ERROR);
                dialog.setMessage("Source folder does not contain any js file(s)");
                dialog.showDialog();
                destBtn.setDisable(true);
            } else {
                srcListView.setItems(srcListItems);
                destBtn.setDisable(false);
            }
        } else {
            log.info("Loading src cancelled");
            srcJs.setText("");
            destJs.setText("");
            DialogFX dialog = new DialogFX(DialogFX.Type.ERROR);
            dialog.setTitle(Constants.TITLE_ERROR);
            dialog.setMessage(Constants.TITLE_ERROR_MSG_SRC);
            dialog.showDialog();
            destBtn.setDisable(true);
            minifyBtn.setDisable(true);
        }
    }

    private void loadDest() {
        final File destDir = destDirChooser.showDialog(borderPane.getScene().getWindow());
        if (destDir != null) {
            destFile = destDir;
            if (jsCompressor != null) {
                jsCompressor.setDestination(destFile);
            }
            destJs.setText(destDir.getAbsolutePath());
            minifyBtn.setDisable(false);
        } else {
            log.info("Loading dest cancelled");
            destJs.setText("");
            DialogFX dialog = new DialogFX(DialogFX.Type.ERROR);
            dialog.setTitle("Error");
            dialog.setMessage(Constants.TITLE_ERROR_MSG_DEST);
            dialog.showDialog();
            minifyBtn.setDisable(true);
        }
    }

}
