/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.minifyfx;

/**
 *
 * @author AshishRAgre
 */
public class Constants {

    public static final String LOAD_JAR = "Load Jar";
    public static final String SRC = "src";
    public static final String DEST = "dest";
    public static final String MINIFY = "Minify";
    public static final String OPEN_JAR = "Open Jar";
    public static final String ABOUT = "About";
    public static final String HOME_DIR = "user.home";
    public static final String JAVA_CMD = "java";
    public static final String _JAR = "-jar";
    public static final String _O = "-o";
    public static final String ERROR_PROCESSING = "Error processing";
    public static final String PROCESSED = "Processed";
    public static final String TITLE_PROGRESS = "Progress";
    public static final String TITLE_INFORMATION = "Information";
    public static final String TITLE_ERROR = "Error";
    public static final String TITLE_ERROR_MSG_SRC = "Please select javascript source directory";
    public static final String TITLE_ERROR_MSG_DEST = "Please select destination directory for minified javascript files";
    public static final String YUI_COMPRESSOR = "YUI Compressor GUI";
    public static final String DOT_JAR = ".jar";
    public static final String DOT_JS = ".js";
    public static final String PATH_TO_YUI_COMPRESSOR = "Path to YUI comprocesor jar";
    public static final String JS_SRC_FOLDER = "JS source folder";
    public static final String JS_DEST_FOLDER = "JS destination folder";
}
