/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.minifyfx;

import java.net.URL;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author AshishRAgre
 */
public class ProgressDialog extends Stage {

    private BorderPane layout;
    private ProgressIndicator progressIndicator;
    private TextArea status;

    public ProgressDialog(boolean isModal, String title) {
        super();
        setResizable(false);
        initModality(Modality.APPLICATION_MODAL);
        setTitle(title);
        layout = new BorderPane();
        progressIndicator = new ProgressIndicator(0);
        status = new TextArea();
        Scene scene = new Scene(layout);
        setScene(scene);
        initLayout();
    }

    private void initLayout() {
        layout.setPadding(new Insets(20, 20, 20, 20));
        layout.setCenter(progressIndicator);
        layout.setBottom(status);
    }

    protected ProgressIndicator getProgressIndicator() {
        return this.progressIndicator;
    }

    protected TextArea getStatus() {
        return this.status;
    }
}
