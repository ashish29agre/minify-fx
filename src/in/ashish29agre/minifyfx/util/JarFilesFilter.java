/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.minifyfx.util;

import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * @author AshishRAgre
 */
public class JarFilesFilter implements FilenameFilter {

    private String ext;
    private boolean isJar;

    public JarFilesFilter(String ext, boolean isJar) {
        this.ext = ext;
        this.isJar = isJar;
    }

    @Override
    public boolean accept(File dir, String name) {
        if (this.isJar) {
            return name.endsWith(ext) && name.startsWith("yuicompressor");
        } else {
            return name.endsWith(ext);
        }
    }

}
