/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.minifyfx;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;

/**
 *
 * @author AshishRAgre
 */
public class JsCompressor extends Service<Integer> {

    private static Logger log = Logger.getLogger(JsCompressor.class.getName());
    private ObservableList<File> srcListItems;
    private File destinationDir;
    private ProcessBuilder compressor;
    private String jarPath;
    private ProgressIndicator progressIndicator;
    private TextArea status;

    public JsCompressor(ObservableList<File> srcListItems) {
        this.srcListItems = srcListItems;
        log.setLevel(Level.OFF);
    }

    public void setProgressIndicator(ProgressIndicator progressIndicator) {
        this.progressIndicator = progressIndicator;
    }

    public void setStatus(TextArea status) {
        this.status = status;
    }

    public void setDestination(File destFile) {
        this.destinationDir = destFile;
        log.info("Incoming param " + destFile.getAbsolutePath());
    }

    public void bindStatus() {
        this.messageProperty().addListener(msgChangeListener);
    }

    private void unBindStatus() {
        this.messageProperty().removeListener(msgChangeListener);
    }

    public void bindProgress() {
        progressIndicator.progressProperty().unbind();
        progressIndicator.setProgress(0);
        progressIndicator.progressProperty().bind(this.progressProperty());
    }

    public void unBindProgress() {
        progressIndicator.progressProperty().unbind();
    }

    @Override
    protected Task<Integer> createTask() {
        Task<Integer> jsCompressorTask = new Task<Integer>() {
            List<File> processTrack;

            @Override
            protected void succeeded() {
                reset();
                JsCompressor.this.unBindProgress();
                JsCompressor.this.unBindStatus();
                progressIndicator.setProgress(100);
            }

            @Override
            protected Integer call() {
                int i = 0;
                int failed = 0;
                for (File currentFile : srcListItems) {
                    try {
                        int indexOfDot = currentFile.getName().indexOf('.');
                        String arg1 = currentFile.getAbsolutePath();
                        String arg2 = destinationDir.getAbsolutePath() + "//" + currentFile.getName().substring(0, indexOfDot) + "-min.js";
                        String fullCommand = Constants.JAVA_CMD + Constants._JAR + jarPath + arg1 + Constants._O + arg2;
                        log.info("Full command " + fullCommand);
                        compressor = new ProcessBuilder();
                        List<String> commands = new ArrayList<String>();
                        commands.add(Constants.JAVA_CMD);
                        commands.add(Constants._JAR);
                        commands.add(jarPath);
                        commands.add(arg1);
                        commands.add(Constants._O);
                        commands.add(arg2);
                        compressor.command(commands);
                        Process p = compressor.start();
                        p.waitFor();
                        InputStream is = p.getErrorStream();
                        InputStreamReader isr = new InputStreamReader(is);
                        BufferedReader br = new BufferedReader(isr);
                        String line;
                        String errorMsg = "";
                        while ((line = br.readLine()) != null) {
                            errorMsg += line;
                            errorMsg += "\n";
                        }
                        updateMessage(Constants.PROCESSED + " " + currentFile.getName());
                        if (errorMsg != "") {
                            String err = Constants.ERROR_PROCESSING + " " + currentFile.getName();
                            err += errorMsg;
                            updateMessage(err);

                            ++failed;
                        }
                        updateProgress(++i, srcListItems.size());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return failed;
            }
        };
        return jsCompressorTask;

    }

    public void setJarPath(String jarPath) {
        this.jarPath = jarPath;
    }

    private ChangeListener msgChangeListener = new ChangeListener() {

        @Override
        public void changed(ObservableValue ov, Object t, Object t1) {
            status.appendText(t1 + "\n");
        }
    };

}
